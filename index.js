const process = require('process')
const { spawn } = require('child_process')

function create(child) {
    child.on('exit', (code, signal) => {
        console.log(`child process exited with code ${code} and signal ${signal}`)

        setTimeout(() => {
            instance()
        }, 60000)
    })

    child.on('error', (error) => {
        console.error('child error', error)
    })

    child.stdout.on('data', (data) => {
        process.stdout.write(data.toString())
    })

    child.stderr.on('data', (data) => {
        process.stdout.write(data.toString())
    })
}

function instance() {
    const child = spawn('node', ['instance.js'], { cwd: process.cwd() })
    create(child)
}

instance()
