const process = require('process')
const mineflayer = require('mineflayer')
const config = require('./config.json')

let bot = mineflayer.createBot(config)

bot.on('error', (err) => console.log(err))

bot.once('login', () => {
    console.log('bot:login')
})

bot.once('spawn', () => {
    console.log('bot:spawn')

    setTimeout(() => {
        console.log('bot:chat:survival')
        bot.chat('/server survival')
    }, 10000)
})

bot.on('chat', (username, message) => {
    message = JSON.stringify(message).replace(/['"]+/g, '');

    console.log(`bot:chat - ${username}: ${message}`)

    if (username === 'maartenvr98' || username === 'Maarten') {

        const command = message.replace(/ .*/,'');

        switch(command) {
            case 'move':

                bot.setControlState('forward', true);
                setTimeout(function () {
                    bot.clearControlStates();
                    bot.chat('/home home');
                }, 5000)

                break;
            case 'summon':

                bot.chat('/tpa maartenvr98')

                break;
            case 'chat':

                const messageToSend = message.replace('chat ', '');

                bot.chat(messageToSend);

                break;
            case 'accept':

                bot.chat('/tpaccept')

                break;
        }
    }
})

bot.on('kicked', (reason) => {
    console.log('bot:kicked', reason)

    process.exit()
})

bot.on('whisper', (username, message, translate, jsonMsg, matches) => {
    console.log('bot:whisper', username, message)

    if (username === 'maartenvr98') {
        if (message === 'summon') bot.chat('/tpa BobDeBouwer1337')
        else if (message === 'accept') bot.chat('/tpaccept')
    }
})
